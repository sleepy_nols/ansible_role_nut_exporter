# Ansible Role NUT Exporter

Ansible role to install and configure [Prometheus NUT exporter](https://github.com/DRuggeri/nut_exporter) on Debian-like systems.

<a href="https://gitlab.com/sleepy_nols/ansible_role_nut_exporter/-/pipelines/latest">
<img alt="ansible-lint pipeline" src="https://gitlab.com/sleepy_nols/ansible_role_nut_exporter/badges/main/pipeline.svg"/>
</a>
<br>

## Role Variables and Defaults

See `defaults/main.yml`

### Exporter Variables
Exporter variables are controlled via `nut_exporter_vars_enable`. There are two options on how to query all the parameters of you UPS. Leave the variables blank (`nut_exporter_vars_enable: ""`), or if that does not work, query the UPS for all of its values.

Requirements:
- nut-client installed

```bash
# syntax
upsc upsname[@hostname[:port]]

# example
upsc eaton_ups@10.1.2.3:3493
```
Query the ups for data.

```bash
upsc upsname[@hostname[:port]] | sed 's/: .*$//p' | sed -z 's/\n/,/g'
```
Create a list of supported parameters. (Not perfect, check for leftover commas, remove unneeded parameters)

## Installing
Clone the git repo
```bash
git clone git@gitlab.com:sleepy_nols/ansible_role_nut_exporter.git
```

## Example Playbook
```yml
- hosts: prom-nut-exporters
  roles:
    - ansible_role_nut_exporter
```

## Example Prometheus Scrape Config
```yml
#/etc/prometheus/prometheus.yml
scrape_configs:
  - job_name: nut-exporter
    metrics_path: /ups_metrics
    static_configs:
    - targets:
      - your-nut-exporter-host:9199
      labels:
        ups: "fooups"

```

## Contributing

All contributions are welcome. :)

## License
GPLv3
